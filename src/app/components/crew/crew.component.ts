import { Component, OnInit, ViewChild, ElementRef,Renderer2  } from '@angular/core';
import {FormGroup, FormControl, Validators } from '@angular/forms';
import { NotificationService } from '../..//notification.service';
import { Worker } from './worker';
import { Benefit } from './benefit';
import {WorkerBenefits} from './workerBenefits';
import { CrewService } from '../../crew.service';

const EDIT_LABEL = "Edit Crew";
const ADD_LABEL = "Add Crew";
const EDIT_BUTTON = "Edit";
const ADD_BUTTON = "Add";
const FRINGE = "Fringe";
const Burden = "Burden";

@Component({
  selector: 'app-crew',
  templateUrl: './crew.component.html',
  styleUrls: ['./crew.component.css']
})
export class CrewComponent implements OnInit {

  constructor(private renderer: Renderer2, private notifyService: NotificationService, private crewService: CrewService) {}
  crewList: Worker[] = [];
  benefitsList: Benefit[];
  currentBenefitList: Benefit[];
  temporalBenefit: Benefit;
  workerToEdit: Worker;
  waiting = false;
  idCrewToDelete;
  idToEdit: number;
  isEditMode:boolean = false;
  modalLabel: string;
  benefitLabel: string;
  buttonLabel: string;
  addError: boolean = false;
  isEditinBenefits: boolean =false;
  currentBenefits: WorkerBenefits[];
  benefitType: string;
  benefitTag: string;
  workerDesc: string;

  @ViewChild('closeAddModal') private closeAddModal: ElementRef;
  @ViewChild('closeDeleteModal') private closeDeleteModal: ElementRef;
  @ViewChild('closeBenefitModal') private closeBenefitModal: ElementRef;
  @ViewChild('fringeLink') private fringeLink: ElementRef;
  @ViewChild('crewLink') private crewLink: ElementRef;
  @ViewChild('burdenLink') private burdenLink: ElementRef;
  @ViewChild('divCrew') private divCrew: ElementRef;
  @ViewChild('divFringe') private divFringe: ElementRef;
  

  
  workerForm = new FormGroup({
      description: new FormControl('', Validators.required),
      laborClass: new FormControl('', Validators.required),
      standarWage: new FormControl('', Validators.required),
      additionalFee: new FormControl('', Validators.required),
  });

  benefitForm = new FormGroup({
    benefit: new FormControl('',Validators.required ),
    percentage: new FormControl('', Validators.required),
  });

  ngOnInit(): void {
    this.crewService.getListCrew().subscribe(resp => {
      this.crewList = resp;
    });

    this.crewService.getBenefits().subscribe(resp => {
      this.benefitsList = resp;
    });
  }

  addCrew(): void {  
    this.waiting = true;
    if(this.workerForm.valid || this.isEditinBenefits){
      let newWorker =   this.getNewWorker();
      if(!this.isEditMode){
        this.crewService.addCrew(newWorker.toJsonBody()).subscribe(resp => {
          if(resp.status=="OK"){
            this.crewList.push(resp.data);
            this.waiting = false;
            this.closeModal();
            this.notifyService.showSuccess("Crew saved","Crew");
          }else{
            this.waiting = false;
            if(resp.status=='409'){
              this.addError=true;
            }else{
              this.notifyService.showError("It was an error, Try later","Crew");
              this.closeModal();
            }
            
          }
        });
      }else{
        if(!this.isEditinBenefits){
          this.workerToEdit = this.getEditedWorker();
        }else{
          this.workerToEdit.joinBenefits();
        }
        let body = this.workerToEdit.toJsonBody();
        this.crewService.editCrew(body, this.workerToEdit.idWorker).subscribe(resp => {
          if(resp.status=="OK"){
            this.repleaceWorkerinWorkerlList(resp.data);
            this.waiting = false;
            this.closeModal();
            this.notifyService.showSuccess("Classification updated","Classification");
          }else{
            this.waiting = true;
            this.notifyService.showError("It was an error, Try later","Classification");
              this.closeModal();
          }
        });
      }
      
         
    }else{
      this.workerForm.markAllAsTouched();
      this.waiting = false;

    }
    
  }

  deleteCrew(): void {
    this.waiting = true;
    this.crewService.deleteCrew(this.idCrewToDelete).subscribe(resp=>{
      if(resp.status=="OK"){
        const pos = this.crewList.findIndex(crew => crew.idWorker === this.idCrewToDelete);
        this.crewList.splice(pos,1);
        this.waiting = false;
        this.closeDeleteModal.nativeElement.click();
        this.idCrewToDelete=null;
        this.notifyService.showSuccess("Crew deleted","Crew");
      }else{
        this.waiting = false;
        this.closeDeleteModal.nativeElement.click();
        this.idCrewToDelete=null;
        this.notifyService.showError("It was an error, Try later","Crew");
      }
      
    });
   }

   getNewWorker(): Worker{
    let newWorker =  new Worker(null, this.workerForm.controls.description.value, 
      this.workerForm.controls.laborClass.value,
      this.workerForm.controls.standarWage.value,
      this.workerForm.controls.additionalFee.value,null);
      if(this.workerToEdit != null){
        newWorker.benefits = [];
        if(this.workerToEdit.fringeBenefits != null){
          newWorker.benefits.push.apply(newWorker.benefits, this.workerToEdit.fringeBenefits);
        }
        if(this.workerToEdit.burdenBenefits != null){
          newWorker.benefits.push.apply(newWorker.benefits, this.workerToEdit.burdenBenefits);

        }
        this.currentBenefits = null;
        this.benefitForm.reset();
      };
     
      return newWorker;
  }

  getEditedWorker(): Worker{
    return  new Worker(this.workerToEdit.idWorker, this.workerForm.controls.description.value, 
      this.workerForm.controls.laborClass.value,
      this.workerForm.controls.standarWage.value,
      this.workerForm.controls.additionalFee.value,this.workerToEdit.benefits)
  }

  closeModal(): void {
    this.closeAddModal.nativeElement.click();
    this.closeBenefitModal.nativeElement.click();
    this.isEditinBenefits= false;
    this.isEditMode = false;
    this.cleanBenefitForm();
  }

  resetForm(): void {
    this.workerForm.reset();
    this.addError = false;
  }

  openModal(idn: number, isEdit: boolean):void {
    this.resetForm();
    this.isEditMode = isEdit;
    this.waiting = false;
    
    if(isEdit){
      this.fectClassToEdit(idn);
      this.setformFields();
      this.idToEdit = idn;
      this.modalLabel = EDIT_LABEL;
      this.buttonLabel = EDIT_BUTTON;
    }else{
      this.modalLabel = ADD_LABEL;
      this.buttonLabel = ADD_BUTTON;
      this.workerToEdit = null;
      this.currentBenefits = null;
      this.benefitForm.reset();

    }
  }

  editBenefits(id : number, type: string){
    this.isEditMode = true;
    this.isEditinBenefits = true;
    this.fectClassToEdit(id);
    this.workerToEdit.separateBenefits();
    this.benefitType = type;
    this.workerDesc = this.workerToEdit.description;
    if(type=="fringe"){
      this.benefitTag = "Fringe";
      this.currentBenefits =  this.workerToEdit.fringeBenefits;
      this.currentBenefitList =  this.benefitsList.filter(benefit => benefit.benefitType ==="FRINGE");
    }else if(type == "burden"){
      this.currentBenefits =  this.workerToEdit.burdenBenefits;
      this.benefitTag = "Burden";  
      this.currentBenefitList =  this.benefitsList.filter(benefit => benefit.benefitType ==="BURDEN");

    }
  }

  openAddBenefitModal( type: string){
    this.isEditMode = true;
    this.isEditinBenefits = true;
    this.benefitType = type;
    if(type=="fringe"){
      this.benefitTag = "Fringe";
      this.currentBenefitList =  this.benefitsList.filter(benefit => benefit.benefitType ==="FRINGE");
    }else if(type == "burden"){
      this.benefitTag = "Burden";  
      this.currentBenefitList =  this.benefitsList.filter(benefit => benefit.benefitType ==="BURDEN");

    }
  }

  cleanBenefitForm(){
    this.benefitForm.reset();
    this.isEditMode = false;
    this.isEditinBenefits = false;
    this.workerToEdit = null;
  }

  fectClassToEdit(id : number){
    let temp = this.crewList.find(crew => crew.idWorker === id);
    this.workerToEdit = new Worker(temp.idWorker,temp.description, temp.laborClass, temp.standardWage, temp.additionalFee, temp.benefits);

 }

 setformFields(){
  this.workerForm.controls.description.setValue(this.workerToEdit.description);
  this.workerForm.controls.laborClass.setValue(this.workerToEdit.laborClass);
  this.workerForm.controls.standarWage.setValue(this.workerToEdit.standardWage);
  this.workerForm.controls.additionalFee.setValue(this.workerToEdit.additionalFee);
 }

 setCrewToDelete(idCrew: number) {
  this.idCrewToDelete = idCrew;
}

addBenefit(){

  if(this.benefitForm.valid){
    let idBenefit:number = +this.benefitForm.controls.benefit.value;
    this.temporalBenefit = this.benefitsList.find(benefit => benefit.idBenefit === idBenefit);
    let benefit = new WorkerBenefits(this.benefitForm.controls.benefit.value,
    this.benefitForm.controls.percentage.value,this.temporalBenefit.name,
    this.temporalBenefit.description, this.temporalBenefit.benefitType);
    if(this.benefitType == "fringe"){
      if(this.workerToEdit == null){
        this.workerToEdit = new Worker(null,null,null,null,null,null);
      }
      if(this.workerToEdit.fringeBenefits == null){
        this.workerToEdit.fringeBenefits = [];
      }
      this.workerToEdit.fringeBenefits.push(benefit);
      this.currentBenefits = this.workerToEdit.fringeBenefits;
    }else{

      if(this.workerToEdit == null){
        this.workerToEdit = new Worker(null,null,null,null,null,null);
      }
      if(this.workerToEdit.burdenBenefits == null){
        this.workerToEdit.burdenBenefits = [];
      }
      this.workerToEdit.burdenBenefits.push(benefit);
      this.currentBenefits = this.workerToEdit.burdenBenefits;
    }
    
    this.benefitForm.reset();
  }else{
    this.benefitForm.markAllAsTouched();
  }
  
}

deleteBenefit(index: number){
    if(this.benefitType=="fringe"){
      this.workerToEdit.fringeBenefits.splice(index, 1);
    }else{
      this.workerToEdit.burdenBenefits.splice(index, 1);
    }
}

repleaceWorkerinWorkerlList(workerP: Worker) {
  const pos = this.crewList.findIndex(crew => crew.idWorker === workerP.idWorker);
  this.crewList[pos] = workerP;
}

navigateAddModal(benefit: string){
  if(benefit == 'crew'){
    this.renderer.addClass(this.crewLink.nativeElement, "active");
    this.renderer.removeClass(this.fringeLink.nativeElement, "active");
    this.renderer.removeClass(this.burdenLink.nativeElement, "active");
    this.renderer.addClass(this.divCrew.nativeElement, "visible");
    this.renderer.removeClass(this.divCrew.nativeElement, "invisible");
    this.renderer.removeClass(this.divFringe.nativeElement, "visible");
    this.renderer.addClass(this.divFringe.nativeElement, "invisible");
    this.benefitForm.reset();
  } else if(benefit == 'fringe'){
    this.renderer.removeClass(this.crewLink.nativeElement, "active");
    this.renderer.addClass(this.fringeLink.nativeElement, "active");
    this.renderer.removeClass(this.burdenLink.nativeElement, "active");
    this.renderer.addClass(this.divCrew.nativeElement, "invisible");
    this.renderer.addClass(this.divFringe.nativeElement, "visible");
    this.renderer.removeClass(this.divFringe.nativeElement, "invisible");
    this.currentBenefitList =  this.benefitsList.filter(benefit => benefit.benefitType ==="FRINGE");
    this.benefitTag = "Fringe";
    this.benefitForm.reset();    
    this.benefitType="fringe";
    if(this.workerToEdit != null){
      this.currentBenefits =  this.workerToEdit.fringeBenefits;
    }
  }else {
    this.renderer.removeClass(this.crewLink.nativeElement, "active");
    this.renderer.removeClass(this.fringeLink.nativeElement, "active");
    this.renderer.addClass(this.burdenLink.nativeElement, "active");
    this.currentBenefitList =  this.benefitsList.filter(benefit => benefit.benefitType ==="BURDEN");
    this.benefitTag = "Burden";
    if(this.workerToEdit != null){
      this.currentBenefits =  this.workerToEdit.burdenBenefits;
    }
    this.benefitForm.reset();
    this.benefitType="burden";


  }
}

}