import { WorkerBenefits } from './workerBenefits';

export class Worker {
    idWorker: number;
    description: string;
    laborClass: string;
    standardWage: number;
    additionalFee: number;
    benefits: WorkerBenefits[];
    fringeBenefits: WorkerBenefits[];
    burdenBenefits: WorkerBenefits[];


    constructor(idWorker: number, description: string, laborClass: string, standardWage: number,
        additionalFee: number, benefits:  WorkerBenefits[])  {
        this.idWorker = idWorker;
        this.description = description;
        this.laborClass = laborClass;
        this.standardWage = standardWage;
        this.additionalFee = additionalFee;
        this.benefits = benefits;
    }

    separateBenefits(){
       if(this.benefits != null){
        this.fringeBenefits = this.benefits.filter(benefit => benefit.benefitType ==="FRINGE");
        this.burdenBenefits = this.benefits.filter(benefit => benefit.benefitType ==="BURDEN");
       }
       

    }

    joinBenefits(){
        this.benefits = [];
        this.benefits.push.apply(this.benefits, this.fringeBenefits);
        this.benefits.push.apply(this.benefits, this.burdenBenefits);
     }

     toJsonBody(): any {
        
        if(this.benefits == null){
            return {
                description: this.description,
                laborClass: this.laborClass,
                standardWage: this.standardWage,
                additionalFee: this.additionalFee,
                benefits: [],
            }
        }else{
            return {
                description: this.description,
                laborClass: this.laborClass,
                standardWage: this.standardWage,
                additionalFee: this.additionalFee,
                benefits: this.benefits,
            }
        }
        
    }
}
