export class WorkerBenefits {
    idBenefit: string;
    percentage: number;
    name: string;
    description: string;
    benefitType: string;

    constructor(idBenefit:string, percentage: number, name:string, description: string, benefitType: string){
        this.idBenefit = idBenefit;
        this.percentage = percentage;
        this.name = name;
        this.description = description;
        this.benefitType = benefitType;
    }
}