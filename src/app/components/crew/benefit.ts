export class Benefit{
   
  idBenefit: number;
   name: string;
   description: string;
   benefitType: string;
   percentage: string;
}