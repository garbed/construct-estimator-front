export class Material {
  idClassificationMaster: string;
  idMaterialMaster: number;
  classCode: string;
  description: string;
  width: number;
  height: number;
  length: number;
  measuringUnit: string;
  price: number;
  waste: number;
  productionRatePerMan: number;
  pricePerUnit: number;
  taxRate: number;
  color: string;

  constructor(
    $code: string,
    $materialID: string,
    $description: string,
    $width: number,
    $height: number,
    $length: number,
    $measuringUnit: string,
    $price: number,
    $waste: number,
    $productionRatePerMan: number,
    $pricePerUnit: number,
    $taxRate: number,
    pColor: string
  ) {
    this.idClassificationMaster = $code;
    this.classCode = $materialID;
    this.description = $description;
    this.width = $width;
    this.height = $height;
    this.length = $length;
    this.measuringUnit = $measuringUnit;
    this.price = $price;
    this.waste = $waste;
    this.productionRatePerMan = $productionRatePerMan;
    this.pricePerUnit = $pricePerUnit;
    this.taxRate = $taxRate;
    this.color = pColor;
  }

  toJsonBody(): any {
    return {
      idClassificationMaster: this.idClassificationMaster,
      classCode: this.classCode,
      description: this.description,
      width: this.width,
      height: this.height,
      length: this.length,
      measuringUnit: this.measuringUnit,
      price: this.price,
      waste: this.waste,
      productionRatePerMan: this.productionRatePerMan,
      pricePerUnit: this.pricePerUnit,
      taxRate: this.taxRate,
      idMaterialMaster: this.idMaterialMaster,
      color: this.color,
    };
  }
}
