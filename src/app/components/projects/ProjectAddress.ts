export class ProjectAddress {
    street1: String;
    street2: String;
    city: String;
    state: String;
    zipCode: String;
    phoneNumber: String;    

    


  constructor($street1: String,
    $street2: String,
    $city: String,
    $state: String,
    $zipCode: String,
    $phoneNumber: String) {

      this.street1 = $street1;
      this.street2 = $street2;
      this.city = $city;
      this.state = $state;
      this.zipCode = $zipCode;
      this.phoneNumber = $phoneNumber;
	}

  }