import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, Validators,FormGroup } from '@angular/forms';
import { ApiService } from '../../api.service';
import { Project } from './Project';
import { ProjectAddress } from './ProjectAddress';
import { NotificationService } from '../..//notification.service';
import { Router } from '@angular/router';
import { Division } from './Division';
import { DateAdapter } from '@angular/material/core';


const EDIT_PROJECT="Edit Project";
const ADD_PROJECT="Add Project";
const EDIT_BUTTON="Edit";
const ADD_BUTTON="Add";


@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})

export class ProjectsComponent implements OnInit {
  @ViewChild('closeAddModal') private closeAddModal: ElementRef;
  @ViewChild('closeDeleteModal') private closeDeleteModal: ElementRef;

  projects: Project[];
  projectToEdit: Project;
  isEdit = false;
  addError = false;
  idProjectToDelete;
  moddalTittle: string;
  modalButton: string;
  projectName = new FormControl('', Validators.required);
  projectStatus = new FormControl('', Validators.required);
  division = new FormControl('', Validators.required);
  divisionsList: Division[];

  addressForm = new FormGroup({
    street1: new FormControl('', Validators.required),
    street2: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    state: new FormControl('', Validators.required),
    zipCode: new FormControl('', Validators.required),
    customerName: new FormControl('', Validators.required),
    company: new FormControl('', Validators.required),
    phoneNumber: new FormControl('', Validators.required),
    bidDate: new FormControl('', Validators.required),
    startDate: new FormControl('', Validators.required),
    estimatedCompletion: new FormControl('', Validators.required),   
  });

  errorMessage;
  waiting = false;


  constructor(private notifyService : NotificationService,private apiService: ApiService, private router: Router) { }
  ngOnInit(): void {
    this.apiService.getListProyects().subscribe(resp => {
      this.projects = resp;

    });
    this.apiService.getDivisions().subscribe(resp => {
      this.divisionsList = resp;
    });
  }

  closeModal(): void {
    this.closeAddModal.nativeElement.click();
    this.projectName.reset();
  }

  openModal(id: number, isEdit: boolean): void {
    this.projectName.reset();
    this.division.reset();
    this.addressForm.reset();
    this.isEdit = isEdit;
    this.addError = false;
    this.errorMessage="";
    if (isEdit) {
      this.moddalTittle=EDIT_PROJECT;
      this.modalButton = EDIT_BUTTON;
      this.fethProjectToEdit(id);
      this.division.disable();

    }else{
      this.moddalTittle=ADD_PROJECT;
      this.modalButton = ADD_BUTTON;
      this.division.enable();


    }
  }

  fethProjectToEdit(id: number) {
    this.projectToEdit = this.projects.find(project => project.idProject === id);
    this.projectName.setValue(this.projectToEdit.name);
    this.projectStatus.setValue(this.projectToEdit.status);
    this.division.setValue(this.projectToEdit.idDivision);

      this.addressForm.controls.customerName.setValue(this.projectToEdit.customerName);
      this.addressForm.controls.company.setValue(this.projectToEdit.company);
      this.addressForm.controls.bidDate.setValue(this.projectToEdit.bidDate);
      this.addressForm.controls.startDate.setValue(this.projectToEdit.startDate);
      this.addressForm.controls.estimatedCompletion.setValue(this.projectToEdit.estimatedCompletedDate);
      this.addressForm.controls.street1.setValue(this.projectToEdit.address.street1);
      this.addressForm.controls.street2.setValue(this.projectToEdit.address.street2);
      this.addressForm.controls.city.setValue(this.projectToEdit.address.city);
      this.addressForm.controls.state.setValue(this.projectToEdit.address.state);
      this.addressForm.controls.zipCode.setValue(this.projectToEdit.address.zipCode);
      this.addressForm.controls.phoneNumber.setValue(this.projectToEdit.address.phoneNumber);
      
  }

  repleaceProjectinProjects(projectP: Project) {
    const pos = this.projects.findIndex(project => project.idProject === projectP.idProject);
    this.projects[pos] = projectP;
  }

  getDivisionDescription(idDivisionp: number){
    if(this.divisionsList != undefined){
      const pos = this.divisionsList.findIndex(division =>division.idDivision === idDivisionp);
      return this.divisionsList[pos].description;
    }
   
  }

  addProject(): void {
    this.waiting  = true;
    this.division.enable();
    if (this.projectName.valid && this.division.valid && this.addressForm.valid) {
      if (this.isEdit) {
        this.apiService.editProject(this.projectToEdit.idProject, this.createEditedProject().toJsonBody()).subscribe(resp => {
            this.repleaceProjectinProjects(resp.data);
            this.waiting  = false;
            this.notifyService.showSuccess("Project updated","Projects");
            this.closeModal();
          });
      } else {
        let newProject = this.createNewProject();
        this.apiService.addProject(newProject.toJsonBody())
        .subscribe(resp => {
          if(resp.status=="OK"){
            this.projects.push(resp.data);
            this.waiting  = false;
            this.notifyService.showSuccess("Project saved","Projects");
            this.closeModal();
          }else{
            if(resp.status=='409'){
              this.addError=true;
              this.waiting  = false;
            }else{
              this.notifyService.showError("An error error ocurred, try later","Projects");
              this.waiting  = false;
              this.closeModal();
            }
            
          }
          
        });
      }

    } else {
      this.projectName.markAsTouched();
      this.division.markAsTouched();
      this.addressForm.markAllAsTouched();
      this.waiting  = false;
    }

  }

  setProjectToDelete(idProject: number) {
    this.idProjectToDelete = idProject;
  }

  deleteProject(): void {
    this.waiting  = true;
    this.apiService.deleteProject(this.idProjectToDelete).subscribe(resp=>{
      const pos = this.projects.findIndex(project => project.idProject === this.idProjectToDelete);
      this.projects.splice(pos,1);
      this.closeDeleteModal.nativeElement.click();
      this.idProjectToDelete=null;
      this.waiting  = false;
      this.notifyService.showWarning("Project deleted","Projects");
    });
   }

   setProject(id:number, division: number, projectName: string){
    localStorage.setItem('currentProjectID', JSON.stringify(id));
    localStorage.setItem('currentDivision', JSON.stringify(division));
    localStorage.setItem('projectName', JSON.stringify(projectName));
    this.router.navigate(['projectClass']).then(() => {
      window.location.reload();
    });  }

  createNewProject(): Project{
    let address =  this.createNewAddress();
    return new Project(null,this.projectName.value,
      new Date(),
      'OPEN',
      1,
      this.division.value,
      this.addressForm.controls.customerName.value,
      this.addressForm.controls.company.value,
      this.addressForm.controls.bidDate.value,
      this.addressForm.controls.startDate.value,
      this.addressForm.controls.estimatedCompletion.value,
      address);
  }

  createNewAddress(): ProjectAddress{
    return new ProjectAddress(this.addressForm.controls.street1.value,
      this.addressForm.controls.street2.value,
      this.addressForm.controls.city.value,
      this.addressForm.controls.state.value,
      this.addressForm.controls.zipCode.value,
      this.addressForm.controls.phoneNumber.value);
  }

  createEditedProject(): Project{
    let address =  this.createNewAddress();
    
    return new Project(this.projectToEdit.idProject,this.projectName.value,
      this.projectToEdit.creationDate,
      this.projectStatus.value,
      this.projectToEdit.owner,
      this.division.value,
      this.addressForm.controls.customerName.value,
      this.addressForm.controls.company.value,
      this.addressForm.controls.bidDate.value,
      this.addressForm.controls.startDate.value,
      this.addressForm.controls.estimatedCompletion.value,
      address);
  }
}
