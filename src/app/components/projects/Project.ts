import { ProjectAddress } from './ProjectAddress';

export class Project {
    idProject:number;
    name: String;
    creationDate: Date;
    status: String;
    owner: number;
    idDivision: number;
    customerName: String;
    company: String;
    bidDate: String;
    startDate: String;
    estimatedCompletedDate: String;
    address: ProjectAddress;


  constructor($idProject: number,$name: String, 
    $creationDate: Date,
    $status: String,
    $owner: number,
    $idDivision: number,
    $customerName: String,
    $company: String,
    $bidDate: String,
    $startDate: String,
    $estimatedComDate: String,
    $address: ProjectAddress) {
      this.name = $name;
      this.idDivision = $idDivision;
      this.customerName = $customerName;
      this.company = $company;
      this.bidDate = $bidDate;
      this.startDate = $startDate;
      this.estimatedCompletedDate = $estimatedComDate;
      this.address = $address;
      this.creationDate = $creationDate;
      this.status = $status;
      this.owner = $owner;

  }

  toJsonBody(): any {
    return {
      name: this.name,
      owner: this.owner,
      status: this.status,
      creationDate: new Date(),
      idDivision: this.idDivision,
      customerName: this.customerName,
      company: this.company,
      bidDate: this.bidDate,
      startDate: this.startDate,
      estimatedCompletedDate: this.estimatedCompletedDate,
      address: this.address
  }
    
}
  
  }