export class MortarMaterial{
   id: number; 
   qty: number;
   description: string;
   unitPrice: number;
   price:number;

   constructor(id: number, qty: number, description: string, unitPrice: number, pricep: number){
      this.id = id;
      this.qty = qty;
      this.description = description;
      this.unitPrice = unitPrice;
      this.price = pricep;
   }

  
  }