import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MortarComponent } from './mortar.component';

describe('MortarComponent', () => {
  let component: MortarComponent;
  let fixture: ComponentFixture<MortarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MortarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MortarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
