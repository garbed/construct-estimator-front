import { MortarMaterial } from './MortarMaterial';

export class Mortar{
    idMortarMaster: number;
    mortarCode: string; 
    description: string;
    color: string;
    mortarMeasuringUnit: string;
    mortarMaterialsMasterDTOS: MortarMaterial[];

    constructor(mortarId: string, description: string, color: string, mortarMaterials: MortarMaterial[], measuringUnitp: string){
       this.description = description;
       this.color = color;
       this.mortarCode = mortarId;
       this.mortarMaterialsMasterDTOS = mortarMaterials;
       this.mortarMeasuringUnit = measuringUnitp;
    }

    toJsonBody(): any {
        return {
            description: this.description,
            mortarCode: this.mortarCode,
            color: this.color,
            idMortarMaster: this.idMortarMaster,
            mortarMaterialsMasterDTOS: this.mortarMaterialsMasterDTOS,
            mortarMeasuringUnit: this.mortarMeasuringUnit
        }
        
    }

}