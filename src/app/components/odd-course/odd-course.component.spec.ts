import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OddCourseComponent } from './odd-course.component';

describe('OddCourseComponent', () => {
  let component: OddCourseComponent;
  let fixture: ComponentFixture<OddCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OddCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OddCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
