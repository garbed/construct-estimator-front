import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialsClasificationsComponent } from './materials-clasifications.component';

describe('MaterialsClasificationsComponent', () => {
  let component: MaterialsClasificationsComponent;
  let fixture: ComponentFixture<MaterialsClasificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialsClasificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialsClasificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
