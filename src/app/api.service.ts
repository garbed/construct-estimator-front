import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Response } from './Response';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  url = environment.urlBack;

  constructor(private httpClient: HttpClient) {}

  public getToken(): Observable<string> {
    const body = { username: 'ask', password: 'p4ssw0rd' };

    var subject = new Subject<string>();
    let token: string;

    this.httpClient
      .post(`${this.url}/login`, body, { observe: 'response' })
      .subscribe((resp) => {
        token = resp.headers.get('authorization');
        subject.next(token);
      });

    return subject.asObservable();
  }

  public addProject(body: any): Observable<Response> {
    
    var subject = new Subject<Response>();

    this.getToken().subscribe((res) => {
      const headers = new HttpHeaders().set('authorization', res);
      this.httpClient
        .post(`${this.url}/projects`, body, { headers: headers })
        .subscribe(
          (data) => {
            var result = new Response();
            result.status = 'OK';
            result.data = data;
            subject.next(result);
          },
          (error) => {
            var result = new Response();
            result.status = error.status;
            subject.next(result);
          }
        );
    });
    return subject;
  }

  public editProject(
    idProject: number,
    body: any
  ): Observable<Response> {
   
    var subject = new Subject<any>();
    this.getToken().subscribe((res) => {
      const headers = new HttpHeaders().set('authorization', res);
      this.httpClient
        .put(`${this.url}/projects/` + idProject, body, { headers: headers })
        .subscribe(
          (data) => {
            var result = new Response();
            result.status = 'OK';
            result.data = data;
            subject.next(result);
          },
          (error) => {
            var result = new Response();
            if (error.status == 409) {
              result.status = '409';
              result.errorMessage =
                'Error: That project name is taken. Try another';
              subject.next(result);
            }
          }
        );
    });
    return subject;
  }

  public getListProyects(): Observable<any> {
    var subject = new Subject<any>();
    this.getToken().subscribe((res) => {
      const headers = new HttpHeaders().set('authorization', res);
      this.httpClient
        .get(`${this.url}/projects`, { headers: headers })
        .subscribe((data) => {
          subject.next(data);
        });
    });
    return subject.asObservable();
  }

  public getListClassificationMaster(): Observable<any> {
    var subject = new Subject<any>();
    this.getToken().subscribe((res) => {
      const headers = new HttpHeaders().set('authorization', res);
      this.httpClient
        .get(`${this.url}/classifications-master`, { headers: headers })
        .subscribe((data) => {
          subject.next(data);
        });
    });
    return subject.asObservable();
  }

  public deleteProject(idProject: number): Observable<any> {
    var subject = new Subject<any>();
    this.getToken().subscribe((res) => {
      const headers = new HttpHeaders().set('authorization', res);
      this.httpClient
        .delete(`${this.url}/projects/` + idProject, { headers: headers })
        .subscribe((data) => {
          subject.next(data);
        });
    });
    return subject;
  }

  public addClassification(body: any): Observable<Response> {
    var subject = new Subject<Response>();

    this.getToken().subscribe((res) => {
      const headers = new HttpHeaders().set('authorization', res);
      this.httpClient
        .post(`${this.url}/classifications-master?hasProject=false`, body, {
          headers: headers,
        })
        .subscribe(
          (data) => {
            var result = new Response();
            result.status = 'OK';
            result.data = data;
            subject.next(result);
          },
          (error) => {
            var result = new Response();
            console.log(error.message);
            result.status = error.status;
            subject.next(result);
          }
        );
    });
    return subject;
  }

  public deleteClasification(idClass: number): Observable<any> {
    var subject = new Subject<any>();
    this.getToken().subscribe(
      (res) => {
        const headers = new HttpHeaders().set('authorization', res);
        this.httpClient
          .delete(
            `${this.url}/classifications-master/` +
              idClass +
              `?hasProject=false`,
            { headers: headers }
          )
          .subscribe((data) => {
            var result = new Response();
            result.status = 'OK';
            result.data = data;
            subject.next(result);
          },
          (error) => {
            var result = new Response();
            console.log(error.status);
            result.status = error.status;
            subject.next(result);
          });
      }
    );
    return subject;
  }

  public editClassification(body: any, idClass: number): Observable<Response> {
    var subject = new Subject<any>();
    this.getToken().subscribe((res) => {
      const headers = new HttpHeaders().set('authorization', res);
      this.httpClient
        .put(
          `${this.url}/classifications-master/` + idClass + `?hasProject=false`,
          body,
          { headers: headers }
        )
        .subscribe(
          (data) => {
            var result = new Response();
            result.status = 'OK';
            result.data = data;
            subject.next(result);
          },
          (error) => {
            var result = new Response();
            console.log(error.message);
            result.status = error.status;
            subject.next(result);
          }
        );
    });
    return subject;
  }

  public getDivisions(): Observable<any> {
    var subject = new Subject<any>();
    this.getToken().subscribe((res) => {
      const headers = new HttpHeaders().set('authorization', res);
      this.httpClient
        .get(`${this.url}/divisions`, { headers: headers })
        .subscribe((data) => {
          subject.next(data);
        });
    });
    return subject.asObservable();
  }


}
