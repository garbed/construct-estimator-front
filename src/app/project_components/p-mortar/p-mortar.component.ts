import { Component, OnInit , ViewChild, ElementRef} from '@angular/core';
import {FormGroup, FormControl, Validators } from '@angular/forms';
import { Mortar } from './Mortar';
import { MortarMaterial } from './MortarMaterial';
import { ProjectMortarService } from '../..//project.mortar.service';
import { NotificationService } from '../..//notification.service';


@Component({
  selector: 'app-p-mortar',
  templateUrl: './p-mortar.component.html',
  styleUrls: ['./p-mortar.component.css']
})
export class PMortarComponent implements OnInit {

  public selectedColor: string = '#FFFFFF';
  public prevColor: string;
  mortarList: Mortar[];
  buttonLabel: string;
  waiting: boolean = false;
  modalLabel: string;
  mortarMaterialsList: MortarMaterial[]=[];
  addError: boolean = false;
  mortarIdToDelete: number;
  mortarToEdit: Mortar;
  isEditing: boolean = false;
  currentProjectID: number;
  totalMat:number = 0;
  @ViewChild('checkbox') private checkbox: ElementRef;
  @ViewChild('closeDeleteModal') private closeDeleteModal: ElementRef;
  @ViewChild('closeAddModal') private closeAddModal: ElementRef;


  constructor(private mortarService: ProjectMortarService, private notifyService: NotificationService) { }

  ngOnInit(): void {
    this.currentProjectID = JSON.parse(localStorage.getItem('currentProjectID'));

    this.mortarService.getListMortar(this.currentProjectID).subscribe(resp => {
      this.mortarList = resp;
    });
  }

  mortarForm = new FormGroup({
    mortarId: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    measuringUnit: new FormControl('', Validators.required)
});

marterialForm = new FormGroup({
  unitPrice: new FormControl('', Validators.required),
  description: new FormControl('', Validators.required),
  qty: new FormControl('', Validators.required),
  price:  new FormControl({value: '', disabled: true})
});

  openModal(idn: number, isEdit: boolean):void {
    this.waiting = false;
    this.marterialForm.reset();
    this.mortarForm.reset();
    this.totalMat = 0;
    if(!isEdit){
      this.buttonLabel = "Add Mortar";
      this.modalLabel = "Add Mortar";
      this.mortarMaterialsList=null;
      this.marterialForm.controls.unitPrice.setValue(0.0);
      this.marterialForm.controls.qty.setValue(0);
      this.marterialForm.controls.price.setValue(0);
    }else{
      this.buttonLabel = "Update Mortar";
      this.modalLabel = "Update Mortar";
      this.isEditing = true;
      this.fetchMortarToEdit(idn);
      this.sumTotalPrice();

    }
    
  }

  fetchMortarToEdit(mid : number){
    this.mortarToEdit = this.mortarList.find(mortar => mortar.idMortar === mid);
    this.mortarForm.controls.mortarId.setValue(this.mortarToEdit.mortarCode);
    this.mortarForm.controls.description.setValue(this.mortarToEdit.description);
    this.mortarForm.controls.measuringUnit.setValue(this.mortarToEdit.mortarMeasuringUnit);
    this.selectedColor = this.mortarToEdit.color;
    this.mortarMaterialsList = this.mortarToEdit.mortarMaterialsDTOS
    ;

  }

  sumTotalPrice(){
    this.mortarMaterialsList.forEach(element => this.totalMat = this.totalMat + element.price);
  }

  setMortarToDelete(idn: number): void {
    this.mortarIdToDelete = idn;
  }

  closeModal(): void {
    this.closeAddModal.nativeElement.click();

  }

  getNewMortar(): Mortar{
    let  newMortar = new Mortar(this.mortarForm.controls.mortarId.value, this.mortarForm.controls.description.value,
     this.selectedColor,
      this.mortarMaterialsList, this.currentProjectID, this.mortarForm.controls.measuringUnit.value);
      this.mortarMaterialsList = [];
      return newMortar;
  }

  addMortar(): void {
     if(this.mortarForm.valid){
       let newMortar = this.getNewMortar();
       if(!this.isEditing){
        this.mortarService.addMortar(newMortar.toJsonBody()).subscribe(resp => {
          if(resp.status=="OK"){
            this.mortarList.push(resp.data);
            this.waiting = false;
            this.closeModal();
            this.notifyService.showSuccess("Mortar saved","Mortar");
          }else{
            this.waiting = false;
            if(resp.status=='409'){
              this.addError=true;
            }else{
              this.notifyService.showError("It was an error, Try later","Mortar");
              this.closeModal();
            }
            
          }
        });
       }else{
        this.mortarService.editMortar(newMortar.toJsonBody(), this.mortarToEdit.idMortar).subscribe(resp => {
          if(resp.status=="OK"){
            this.repleaceMortarinMortarList(resp.data);
            this.waiting = false;
            this.closeModal();
            this.notifyService.showSuccess("Mortar updated","Mortar");
          }else{
            this.waiting = true;
            this.notifyService.showError("It was an error, Try later","Mortar");
              this.closeModal();
          }
        });
       }
     }else{
      this.mortarForm.markAllAsTouched();
     }
  }

  repleaceMortarinMortarList(mortarP: Mortar) {
    const pos = this.mortarList.findIndex(mortar => mortar.idMortar === mortarP.idMortar);
    this.mortarList[pos] = mortarP;
  }

  openMaterialsModel(mortarID: number): void {
    this.totalMat=0;
    this.mortarToEdit = this.mortarList.find(mortar => mortar.idMortar === mortarID);
    this.mortarMaterialsList = this.mortarToEdit.mortarMaterialsDTOS;
    this.sumTotalPrice();
  }

  addMaterial(): void {
    if(this.marterialForm.valid){
      if(this.mortarMaterialsList==null){
           this.mortarMaterialsList=[];
      }
       this.mortarMaterialsList.push(new MortarMaterial(null, 
        this.marterialForm.controls.qty.value,this.marterialForm.controls.description.value, 
        this.marterialForm.controls.unitPrice.value, this.marterialForm.controls.price.value));
        this.totalMat = this.totalMat +this.marterialForm.controls.price.value;
        this.marterialForm.reset();
    }else{
      this.marterialForm.markAllAsTouched();
    }
  }

  onCheckboxChange(e): void {
    if(e.target.checked){
      this.prevColor = this.selectedColor;
      this.selectedColor="#FFFFFF80";
    }else{
      this.selectedColor=this.prevColor;
    }
  }

  deleteMaterial(index: number){
    let mortarMaterialDeleted: MortarMaterial[] = this.mortarMaterialsList.splice(index, 1);
    let mortarMatDel:MortarMaterial = mortarMaterialDeleted[0];
    this.totalMat = this.totalMat - mortarMatDel.price;
}


  public onChangeColor( data: any): void {
    this.selectedColor = data.color;
    this.checkbox.nativeElement.checked = false;
  }

  deleteMortar(): void {
    this.waiting = true;
    this.mortarService.deleteMortar(this.mortarIdToDelete).subscribe(resp=>{
      if(resp.status=="OK"){
        const pos = this.mortarList.findIndex(mortar => mortar.idMortar === this.mortarIdToDelete);
        this.mortarList.splice(pos,1);
        this.waiting = false;
        this.closeDeleteModal.nativeElement.click();
        this.mortarIdToDelete=null;
        this.notifyService.showWarning("Mortar deleted","Mortar");
      }else{
        this.waiting = false;
        this.closeDeleteModal.nativeElement.click();
        this.mortarIdToDelete=null;
        this.notifyService.showError("It was an error, Try later","Mortar");

      }
      
    });
   }

   calculatePrice(): void{
    let price: number;
    price = this.marterialForm.controls.qty.value * this.marterialForm.controls.unitPrice.value;
    this.marterialForm.controls.price.setValue(price);
   }

}
