import { MortarMaterial } from './MortarMaterial';

export class Mortar{
    idMortar: number;
    mortarCode: string; 
    description: string;
    color: string;
    idProject: number;
    mortarMeasuringUnit: string;
    mortarMaterialsDTOS: MortarMaterial[];

    constructor(mortarId: string, description: string, color: string, mortarMaterials: MortarMaterial[],
        pidProject:number, pmeasuringUnit: string){
       this.description = description;
       this.color = color;
       this.mortarCode = mortarId;
       this.mortarMaterialsDTOS = mortarMaterials;
       this.idProject = pidProject;
       this.mortarMeasuringUnit = pmeasuringUnit;
    }

    toJsonBody(): any {
        return {
            description: this.description,
            mortarCode: this.mortarCode,
            color: this.color,
            idMortar: this.idMortar,
            mortarMaterialsDTOS: this.mortarMaterialsDTOS,
            idProject : this.idProject,
            mortarMeasuringUnit: this.mortarMeasuringUnit
        }
        
    }

}