import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PMortarComponent } from './p-mortar.component';

describe('PMortarComponent', () => {
  let component: PMortarComponent;
  let fixture: ComponentFixture<PMortarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PMortarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PMortarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
