import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PMaterialsClassificationsComponent } from './p-materials-classifications.component';

describe('PMaterialsClassificationsComponent', () => {
  let component: PMaterialsClassificationsComponent;
  let fixture: ComponentFixture<PMaterialsClassificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PMaterialsClassificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PMaterialsClassificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
