import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ColorPickerService, Cmyk } from 'ngx-color-picker';
import { MaterialClassification } from './MaterialClassification';
import {FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { projectClassService } from '../../project_material_class.service';
import { NotificationService } from '../..//notification.service';
import { Division } from '../../components/materials-clasifications/Division';



  const EDIT_LABEL="Edit Classification";
  const ADD_LABEL="Add Classification";
  const EDIT_BUTTON="Edit";
  const ADD_BUTTON="Add";
@Component({
  selector: 'app-p-materials-classifications',
  templateUrl: './p-materials-classifications.component.html',
  styleUrls: ['./p-materials-classifications.component.css']
})
export class PMaterialsClassificationsComponent implements OnInit {
  clasificationList: MaterialClassification[];
  divisionsList: Division[];
  classToEdit: MaterialClassification;
  @ViewChild('checkbox') private checkbox: ElementRef;
  @ViewChild('closeAddModal') private closeAddModal: ElementRef;
  @ViewChild('closeDeleteModal') private closeDeleteModal: ElementRef;

  constructor(private notifyService : NotificationService, private cpService: ColorPickerService, 
    private formBuilder: FormBuilder, private apiService: projectClassService) {}

  public selectedColor: string = '#FFFFFF';
  public prevColor: string;
  public mockId: number = 0;
  
  waiting = false;
  idclassToDelete;
  idToEdit: number;
  isEditMode:boolean = false;
  modalLabel: string;
  buttonLabel: string;
  addError: boolean = false;
  currentProjectID: number;
  currentDivision: number;
  classForm = new FormGroup({
  classification : new FormControl('', [Validators.required,Validators.maxLength(3)]),
  description : new FormControl('', Validators.required),
  imperialUnit : new FormControl('', Validators.required),
  pieces : new FormControl('', Validators.required),
  unitDescription : new FormControl('', Validators.required),
  defaultUnitType : new FormControl('', Validators.required),
  division : new FormControl('', Validators.required),

  });

  ngOnInit(): void {
    this.currentProjectID = JSON.parse(localStorage.getItem('currentProjectID'));
    this.currentDivision =  JSON.parse(localStorage.getItem('currentDivision'));   

    this.apiService.getListClassification(this.currentProjectID).subscribe(resp => {
      this.clasificationList = resp;
    });

    this.apiService.getDivisions().subscribe(resp => {
      this.divisionsList = resp;
    });
  }
    
 
  public onChangeColor( data: any): void {
    this.selectedColor = data.color;
    this.checkbox.nativeElement.checked = false;
  }

  onCheckboxChange(e): void {
    if(e.target.checked){
      this.prevColor = this.selectedColor;
      this.selectedColor="#FFFFFF80";
    }else{
      this.selectedColor=this.prevColor;
    }
  }
  
  addClassification(): void {  
    this.waiting = true;
    if(this.classForm.valid){
      let newClass =   this.getNewClassification();
      if(!this.isEditMode){
        this.apiService.addClassification(newClass.toJsonBody()).subscribe(resp => {
          if(resp.status=="OK"){
            this.clasificationList.push(resp.data);
            this.waiting = false;
            this.closeModal();
            this.notifyService.showSuccess("Classification saved","Classification");
          }else{
            this.waiting = false;
            if(resp.status=='409'){
              this.addError=true;
            }else{
              this.notifyService.showError("It was an error, Try later","Classification");
              this.closeModal();
            }
            
          }
        });
      }else  {
        this.apiService.editClassification(newClass.toJsonBody(), this.idToEdit).subscribe(resp => {
          if(resp.status=="OK"){
            this.repleaceClassinClassList(resp.data);
            this.waiting = false;
            this.closeModal();
            this.notifyService.showSuccess("Classification updated","Classification");
          }else{
            this.waiting = true;
            this.notifyService.showError("It was an error, Try later","Classification");
              this.closeModal();
          }
        });
      }
      
         
    }else{
      this.classForm.markAllAsTouched();
    }
    
  }

  deleteClassification(): void {
    this.waiting = true;
    this.apiService.deleteClasification(this.idclassToDelete).subscribe(resp=>{
      if(resp.status=="OK"){
        const pos = this.clasificationList.findIndex(project => project.idClassification === this.idclassToDelete);
        this.clasificationList.splice(pos,1);
        this.waiting = false;
        this.closeDeleteModal.nativeElement.click();
        this.idclassToDelete=null;
        this.notifyService.showWarning("Classification deleted","Classification");
      }else{
        this.waiting = false;
        this.closeDeleteModal.nativeElement.click();
        this.idclassToDelete=null;
        if(resp.status == "409"){
          this.notifyService.showError("This classification can't be deleted, because it is been used in Materials Configuration","Classification");
        }else{
          this.notifyService.showError("It was an error, Try later","Classification");
        }
      }
      
    });
   }

  getNewClassification():MaterialClassification{
    return new MaterialClassification(
      this.classForm.controls.classification.value,
      this.classForm.controls.description.value,
      this.selectedColor,
      this.classForm.controls.imperialUnit.value,
      this.classForm.controls.pieces.value,
      this.classForm.controls.unitDescription.value,
      this.classForm.controls.defaultUnitType.value,1,
      this.classForm.controls.division.value,
      this.currentProjectID);
  }

  closeModal(): void {
    this.closeAddModal.nativeElement.click();
  }

  openModal(idn: number, isEdit: boolean):void {
    this.resetForm();
    this.isEditMode = isEdit;
    this.classForm.controls.division.disable();
    if(isEdit){
       console.log(idn);
      this.fectClassToEdit(idn);
      this.idToEdit = idn;
      this.modalLabel = EDIT_LABEL;
      this.buttonLabel = EDIT_BUTTON;
    }else{
      this.modalLabel = ADD_LABEL;
      this.buttonLabel = ADD_BUTTON;
      this.classForm.controls.division.setValue(this.currentDivision);
    }
  }

  fectClassToEdit(pid:number){
     this.classToEdit = this.clasificationList.find(classn => classn.idClassification === pid);
     this.classForm.controls.classification.setValue(this.classToEdit.classIdentifier);
     this.classForm.controls.description.setValue(this.classToEdit.description);
     this.selectedColor = this.classToEdit.color;
     this.classForm.controls.imperialUnit.setValue(this.classToEdit.imperialUnit);
     this.classForm.controls.pieces.setValue(this.classToEdit.piecesUnit);
     this.classForm.controls.unitDescription.setValue(this.classToEdit.unitDescription);
     this.classForm.controls.defaultUnitType.setValue(this.classToEdit.defaultUnitType);
     this.classForm.controls.division.setValue(this.classToEdit.idDivision);

  }

  resetForm(): void {
    this.classForm.reset();
    this.addError = false;
   this.selectedColor = '#FFFFFF';

  }

  setProjectToDelete(idClass: number) {
    this.idclassToDelete = idClass;
  }

 

   repleaceClassinClassList(classificationP: MaterialClassification) {
    const pos = this.clasificationList.findIndex(classification => classification.idClassification === classificationP.idClassification);
    this.clasificationList[pos] = classificationP;
  }

  getDivisionDescription(idDivisionp: number){
    if(this.divisionsList != undefined){
      const pos = this.divisionsList.findIndex(division =>division.idDivision === idDivisionp);
      return this.divisionsList[pos].description;
    }
   
  }

}
