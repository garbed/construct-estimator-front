export class MaterialClassification {
  idClassification:number;
  classIdentifier: string;
  description: string;
  color: string;
  imperialUnit: string;
  piecesUnit: string;
  unitDescription: string;
  defaultUnitType: string;
  idTrade: number;
  idDivision: number;
  idProject: number;

  constructor( pclass: string, pDescription, pColor: string, 
      pImperialUnit: string, pieces: string, pUnitDescription: string, pDefaultUnit: string, pIdTrade,
       pdivision: number, pidProject: number) {
    this.classIdentifier = pclass;
    this.description = pDescription;
    this.color = pColor;
    this.imperialUnit = pImperialUnit;
    this.piecesUnit = pieces;
    this.unitDescription = pUnitDescription;
    this.defaultUnitType = pDefaultUnit;
    this.idTrade = pIdTrade;
    this.idDivision = pdivision;
    this.idProject = pidProject;

   }

   toJsonBody(): any{
    
    return  { 
      classIdentifier:this.classIdentifier,
      description:this.description,
      color:this.color,
      imperialUnit:this.imperialUnit,
      piecesUnit:this.piecesUnit,
      unitDescription:this.unitDescription,
      defaultUnitType:this.defaultUnitType,
      idTrade:this.idTrade,
      idDivision:this.idDivision,
      idProject:this.idProject
     };

   }

      
 
   }