import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PMaterialsComponent } from './p-materials.component';

describe('PMaterialsComponent', () => {
  let component: PMaterialsComponent;
  let fixture: ComponentFixture<PMaterialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PMaterialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PMaterialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
