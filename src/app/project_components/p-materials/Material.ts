export class Material {
    idClassificationMaster: string;
    idMaterial: number;
    classCode: string;
    description: string;
    width: number;
    height: number;
    length: number;
    measuringUnit: string;
    price: number;
    waste: number;
    productionRatePerMan: number;
    pricePerUnit: number;
    taxRate: number;
    color: string;
    idProject: number;
    classificationDTO: any;
  
    constructor(
      $code: string,
      $materialID: string,
      $description: string,
      $width: number,
      $height: number,
      $length: number,
      $measuringUnit: string,
      $price: number,
      $waste: number,
      $productionRatePerMan: number,
      $pricePerUnit: number,
      $taxRate: number,
      pColor: string,
      $idProject: number
    ) {
      this.idClassificationMaster = $code;
      this.classCode = $materialID;
      this.description = $description;
      this.width = $width;
      this.height = $height;
      this.length = $length;
      this.measuringUnit = $measuringUnit;
      this.price = $price;
      this.waste = $waste;
      this.productionRatePerMan = $productionRatePerMan;
      this.pricePerUnit = $pricePerUnit;
      this.taxRate = $taxRate;
      this.color = pColor;
      this.idProject = $idProject;
    }
  
    toJsonBody(): any {
      return {
        idClassificationMaster: this.idClassificationMaster,
        classCode: this.classCode,
        description: this.description,
        width: this.width,
        height: this.height,
        length: this.length,
        measuringUnit: this.measuringUnit,
        price: this.price,
        waste: this.waste,
        productionRatePerMan: this.productionRatePerMan,
        pricePerUnit: this.pricePerUnit,
        taxRate: this.taxRate,
        idMaterialMaster: this.idMaterial,
        color: this.color,
        idProject: this.idProject
      };
    }
  }
  