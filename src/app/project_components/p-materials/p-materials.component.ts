import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Material } from './Material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProjectMaterialService } from '../../project.material.service';
import { projectClassService } from '../../project_material_class.service';
import { NotificationService } from '../..//notification.service';
import { MaterialClassification } from '../p-materials-classifications/MaterialClassification';

const EDIT_LABEL = 'Edit Material';
const ADD_LABEL = 'Add Material';
const EDIT_BUTTON = 'Edit';
const ADD_BUTTON = 'Add';

@Component({
  selector: 'app-p-materials',
  templateUrl: './p-materials.component.html',
  styleUrls: ['./p-materials.component.css']
})
export class PMaterialsComponent implements OnInit {

  materialList: Material[];
  clasificationList: MaterialClassification[];
  materialEdit: Material;
  defaultUnit: string;
  meassuringLabel: string;
  unitFactorLabel: string;
  unitFactorLabelb: string;
  imperialUnit: string;
  overWritePrice: boolean = false;



  @ViewChild('closeAddModal') private closeAddModal: ElementRef;
  @ViewChild('closeDeleteModal') private closeDeleteModal: ElementRef;
  @ViewChild('checkbox') private checkbox: ElementRef;

  constructor(private notifyService: NotificationService, private classService: projectClassService, private materialService: ProjectMaterialService
    ) { }
  waiting = false;
  idMaterialToDelete;
  idToEdit: number;
  isEditMode: boolean = false;
  modalLabel: string;
  buttonLabel: string;
  addError: boolean = false;
  currentProjectID: number;

  materialForm = new FormGroup({
    code: new FormControl('', Validators.required),
    materialID: new FormControl('', [Validators.required, Validators.maxLength(10)]),
    description: new FormControl('', Validators.required),
    width: new FormControl('', Validators.required),
    height: new FormControl('', Validators.required),
    length: new FormControl('', Validators.required),
    measuringUnit: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required,),
    waste: new FormControl('', Validators.required),
    productionRatePerMan: new FormControl('', Validators.required),
    pricePerUnit: new FormControl('', Validators.required),
    taxRate: new FormControl('', Validators.required),
    unitFactor: new FormControl('', Validators.required),
    unitFactorb: new FormControl('', Validators.required),
  });
  public selectedColor: string = '#FFFFFF';
  public prevColor: string;

  ngOnInit(): void {
    this.currentProjectID = JSON.parse(localStorage.getItem('currentProjectID'));

    this.classService.getListClassification(this.currentProjectID).subscribe(resp => {
      this.clasificationList = resp;
    });

    this.materialService.getListMaterials(this.currentProjectID).subscribe(resp => {
      this.materialList = resp;
    });
  }

  public onChangeColor(data: any): void {
    this.selectedColor = data;
    this.checkbox.nativeElement.checked = false;
  }

  onCheckboxChange(e): void {
    if (e.target.checked) {
      this.prevColor = this.selectedColor;
      this.selectedColor = '#FFFFFF80';
    } else {
      this.selectedColor = this.prevColor;
    }
  }

  addMaterial(): void {
    this.waiting = true;
    if (this.materialForm.valid) {
      let newMaterial = this.getNewMaterial();
      if (!this.isEditMode) {
        this.materialService.addMaterials(newMaterial.toJsonBody()).subscribe(resp => {
          if (resp.status == "OK") {
            this.materialList.push(resp.data);
            this.waiting = false;
            this.closeModal();
            this.notifyService.showSuccess("Classification saved", "Classification");
          } else {
            this.waiting = false;
            if (resp.status == '409') {
              this.addError = true;
            } else {
              this.notifyService.showError("It was an error, Try later", "Classification");
              this.closeModal();
              this.notifyService.showSuccess(
                'Classification saved',
                'Classification'
              );
            } 

          }
        });
      } else {
        this.materialService.editMaterial(newMaterial.toJsonBody(), this.idToEdit).subscribe(resp => {
          if (resp.status == "OK") {
            this.repleaceMaterialinMaterialList(resp.data);
            this.waiting = false;
            this.closeModal();
            this.notifyService.showSuccess("Classification updated", "Classification");
          } else {
            this.waiting = true;
            this.notifyService.showError("It was an error, Try later", "Classification");
            this.closeModal();
            }
          });
      }


    } else {
      this.materialForm.markAllAsTouched();
      this.waiting = false;
    }

  }

  deleteClassification(): void {
    this.waiting = true;
    this.materialService.deleteMaterial(this.idMaterialToDelete).subscribe(resp => {
      if (resp.status == "OK") {
        const pos = this.materialList.findIndex(material => material.idMaterial === this.idMaterialToDelete);
        this.materialList.splice(pos, 1);
        this.waiting = false;
        this.closeDeleteModal.nativeElement.click();
        this.idMaterialToDelete = null;
        this.notifyService.showSuccess("Classification deleted", "Materials");
      } else {
        this.idMaterialToDelete = null;
        this.notifyService.showError("It was an error, Try later", "Materials");

  }
});}

  getNewMaterial(): Material {
    return new Material(
      this.materialForm.controls.code.value,
      this.materialForm.controls.materialID.value,
      this.materialForm.controls.description.value,
      this.materialForm.controls.width.value,
      this.materialForm.controls.height.value,
      this.materialForm.controls.length.value,
      this.materialForm.controls.measuringUnit.value,
      this.materialForm.controls.price.value,
      this.materialForm.controls.waste.value,
      this.materialForm.controls.productionRatePerMan.value,
      this.materialForm.controls.pricePerUnit.value,
      this.materialForm.controls.taxRate.value,
      this.selectedColor, this.currentProjectID
    );
  }

  repleaceMaterialinMaterialList(materialP: Material) {
    const pos = this.materialList.findIndex(
      (material) => material.idMaterial === materialP.idMaterial
    );
    this.materialList[pos] = materialP;
  }

  closeModal(): void {
    this.closeAddModal.nativeElement.click();
  }

  resetForm(): void {
    this.materialForm.reset();
    this.addError = false;
    this.selectedColor = '#FFFFFF';
  }

  openModal(idn: number, isEdit: boolean): void {
    this.resetForm();
    this.isEditMode = isEdit;
    this.materialForm.controls.measuringUnit.setValue("IMPERIAL");
    this.materialForm.controls.measuringUnit.disable();

    if (isEdit) {
      this.fectClassToEdit(idn);
      this.idToEdit = idn;
      this.modalLabel = EDIT_LABEL;
      this.buttonLabel = EDIT_BUTTON;
      this.onClassChange(this.materialForm.controls.code.value);
    } else {
      this.modalLabel = ADD_LABEL;
      this.buttonLabel = ADD_BUTTON;
    }
  }
  

  setProjectToDelete(idMaterial: number) {
    this.idMaterialToDelete = idMaterial;
  }

  getUnitFactor() {
    this.materialForm.controls.unitFactor.setValue('');
    if (this.verifyUnits()) {
      this.getPieceSqFt(this.materialForm.controls.width.value, this.materialForm.controls.height.value, this.materialForm.controls.length.value);
      this.getUnitPrice();
    }

  }

  verifyUnits(): boolean {
    if (this.materialForm.controls.code.value != null && this.materialForm.controls.code.value != "") {
      if (this.materialForm.controls.width.value != null && this.materialForm.controls.width.value != "") {
        if (this.materialForm.controls.height.value != null && this.materialForm.controls.height.value != "") {
          if (this.materialForm.controls.length.value != null && this.materialForm.controls.length.value != "") {
            return true;
          }
        }
      }
    } else {
      return false;
    }
  }

  getPieceSqFt(pwidth: number, pheight: number, plength: number) {
    let body = {
      height: pheight,
      width: pwidth,
      length: plength,
      imperialUnit: this.imperialUnit
    }
    this.materialService.getPieceSqft(body).subscribe(resp => {
      if (resp.status == "OK") {
        this.materialForm.controls.unitFactor.setValue(resp.data);
      }
    });

    this.materialService.getSqFitPiece(body).subscribe(resp => {
      if (resp.status == "OK") {
        this.materialForm.controls.unitFactorb.setValue(resp.data);
      }
    });
  }

  onClassChange(value) {
    let idclass: number = +value;
    let classification = this.clasificationList.find(classf => classf.idClassification === idclass);
    this.defaultUnit = classification.defaultUnitType;
    this.imperialUnit = classification.imperialUnit;
    if (this.defaultUnit == "IMPERIAL") {
      if (classification.imperialUnit == 'EACH') {
        this.meassuringLabel = "each";
      }else{
        this.meassuringLabel = "inch";
        this.unitFactorLabel = "pieces/inch";
        this.unitFactorLabelb = "inch/piece";
      }

    }
    this.getUnitFactor();

  }

  getUnitPrice(){
    if (this.defaultUnit == "IMPERIAL") {
      if(this.overWritePrice == false && this.verifyUnits() && this.materialForm.controls.price.value != null && this.materialForm.controls.unitFactorb.value != null){
        this.materialService.getPrice(this.materialForm.controls.unitFactorb.value,
          this.materialForm.controls.price.value).subscribe(resp => {
          this.materialForm.controls.pricePerUnit.setValue(resp);
        });
      }
    }else{
      this.materialForm.controls.pricePerUnit.setValue(this.materialForm.controls.pricePerUnit.value);
    }
      
    
  }
  fectClassToEdit(mid : number){
    this.materialEdit = this.materialList.find(material => material.idMaterial === mid);
    this.materialEdit.idClassificationMaster = this.materialEdit.classificationDTO.idClassification;
    this.materialForm.controls.materialID.setValue(this.materialEdit.classCode);
    this.materialForm.controls.code.setValue(this.materialEdit.idClassificationMaster);
    this.materialForm.controls.description.setValue(
      this.materialEdit.description
    );
    this.materialForm.controls.width.setValue(this.materialEdit.width);
    this.materialForm.controls.height.setValue(this.materialEdit.height);
    this.materialForm.controls.length.setValue(this.materialEdit.length);
    this.materialForm.controls.measuringUnit.setValue(
      this.materialEdit.measuringUnit
    );
    this.materialForm.controls.price.setValue(this.materialEdit.price);
    this.materialForm.controls.waste.setValue(this.materialEdit.waste);
    this.materialForm.controls.productionRatePerMan.setValue(
      this.materialEdit.productionRatePerMan
    );
    this.materialForm.controls.pricePerUnit.setValue(
      this.materialEdit.pricePerUnit
    );
    this.materialForm.controls.taxRate.setValue(this.materialEdit.taxRate);
    this.selectedColor = this.materialEdit.color;
  }

  setOverWrite(e){
    if(e.target.checked){
      this.overWritePrice = true;
    }else{
      this.overWritePrice = false;
      this.getUnitPrice();
    }
  }

}
