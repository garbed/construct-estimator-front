import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Response } from './Response';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MortarService {
    url = environment.urlBack;

    constructor(private httpClient: HttpClient) {}
  
    public getToken(): Observable<string> {
      const body = { username: 'ask', password: 'p4ssw0rd' };
  
      var subject = new Subject<string>();
      let token: string;
  
      this.httpClient
        .post(`${this.url}/login`, body, { observe: 'response' })
        .subscribe((resp) => {
          token = resp.headers.get('authorization');
          subject.next(token);
        });
  
      return subject.asObservable();
    }

    public getListMortar(): Observable<any> {
        var subject = new Subject<any>();
        this.getToken().subscribe((res) => {
          const headers = new HttpHeaders().set('authorization', res);
          this.httpClient
            .get(`${this.url}/mortars-master`, { headers: headers })
            .subscribe((data) => {
              subject.next(data);
            });
        });
        return subject.asObservable();
      }


    public addMortar(body: any): Observable<Response> {
        var subject = new Subject<Response>();
        this.getToken().subscribe((res) => {
          const headers = new HttpHeaders().set('authorization', res);
          this.httpClient
            .post(`${this.url}/mortars-master?hasProject=false`, body, {
              headers: headers,
            })
            .subscribe(
              (data) => {
                var result = new Response();
                result.status = 'OK';
                result.data = data;
                subject.next(result);
              },
              (error) => {
                var result = new Response();
                console.log(error.message);
                result.status = error.status;
                subject.next(result);
              }
            );
        });
        return subject;
      }

      public editMortar(body: any, idmortar: number): Observable<Response> {
        var subject = new Subject<any>();
        this.getToken().subscribe((res) => {
          const headers = new HttpHeaders().set('authorization', res);
          this.httpClient
            .put(
              `${this.url}/mortars-master/` + idmortar + `?hasProject=false`,
              body,
              { headers: headers }
            )
            .subscribe(
              (data) => {
                var result = new Response();
                result.status = 'OK';
                result.data = data;
                subject.next(result);
              },
              (error) => {
                var result = new Response();
                console.log(error.message);
                result.status = error.status;
                subject.next(result);
              }
            );
        });
        return subject;
      }

      public deleteMortar(idMortar: number): Observable<any> {
        var subject = new Subject<any>();
        this.getToken().subscribe(
          (res) => {
            const headers = new HttpHeaders().set('authorization', res);
            this.httpClient
              .delete(
                `${this.url}/mortars-master/` + idMortar + `?hasProject=false`,
                { headers: headers }
              )
              .subscribe((data) => {
                var result = new Response();
                result.status = 'OK';
                result.data = data;
                subject.next(result);
              });
          },
          (error) => {
            var result = new Response();
            console.log(error.message);
            result.status = error.status;
            subject.next(result);
          }
        );
        return subject;
      }

      
}