import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialsClasificationsComponent } from './components/materials-clasifications/materials-clasifications.component';
import { MortarComponent } from './components/mortar/mortar.component';
import { WallComponent } from './components/wall/wall.component';
import { MaterialsComponent } from './components/materials/materials.component';

import { OddCourseComponent } from './components/odd-course/odd-course.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ColorPickerModule } from 'ngx-color-picker';
import { ToastrModule } from 'ngx-toastr';
import { CrewComponent } from './components/crew/crew.component';

// used to create fake backend
import { fakeBackendProvider } from './_helpers';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { HomeComponent } from './home';
import { AdminComponent } from './admin';
import { LoginComponent } from './login';
import { PMaterialsClassificationsComponent } from './project_components/p-materials-classifications/p-materials-classifications.component';
import { PMaterialsComponent } from './project_components/p-materials/p-materials.component';
import { PMortarComponent } from './project_components/p-mortar/p-mortar.component';

@NgModule({
  declarations: [
    AppComponent,
    MaterialsClasificationsComponent,
    MortarComponent,
    WallComponent,
    OddCourseComponent,
    ProjectsComponent,
    HeaderComponent,
    FooterComponent,
    MaterialsComponent,
    CrewComponent,
    HomeComponent,
    AdminComponent,
    LoginComponent,
    PMaterialsClassificationsComponent,
    PMaterialsComponent,
    PMortarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    ColorPickerModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    // provider used to create fake backend
    fakeBackendProvider,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
