import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Response } from './Response';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MaterialService {
  url = environment.urlBack;

  constructor(private httpClient: HttpClient) {}

  public getToken(): Observable<string> {
    const body = { username: 'ask', password: 'p4ssw0rd' };

    var subject = new Subject<string>();
    let token: string;

    this.httpClient
      .post(`${this.url}/login`, body, { observe: 'response' })
      .subscribe((resp) => {
        token = resp.headers.get('authorization');
        subject.next(token);
      });

    return subject.asObservable();
  }

  public getListMaterialsMaster(): Observable<any> {
    var subject = new Subject<any>();
    this.getToken().subscribe((res) => {
      const headers = new HttpHeaders().set('authorization', res);
      this.httpClient
        .get(`${this.url}/materials-master`, { headers: headers })
        .subscribe((data) => {
          subject.next(data);
        });
    });
    return subject.asObservable();
  }

  public addMaterials(body: any): Observable<Response> {
    var subject = new Subject<Response>();
    this.getToken().subscribe((res) => {
      const headers = new HttpHeaders().set('authorization', res);
      this.httpClient
        .post(`${this.url}/materials-master?hasProject=false`, body, {
          headers: headers,
        })
        .subscribe(
          (data) => {
            var result = new Response();
            result.status = 'OK';
            result.data = data;
            subject.next(result);
          },
          (error) => {
            var result = new Response();
            console.log(error.message);
            result.status = error.status;
            subject.next(result);
          }
        );
    });
    return subject;
  }

  public deleteMaterial(idMat: number): Observable<any> {
    var subject = new Subject<any>();
    this.getToken().subscribe(
      (res) => {
        const headers = new HttpHeaders().set('authorization', res);
        this.httpClient
          .delete(
            `${this.url}/materials-master/` + idMat + `?hasProject=false`,
            { headers: headers }
          )
          .subscribe((data) => {
            var result = new Response();
            result.status = 'OK';
            result.data = data;
            subject.next(result);
          });
      },
      (error) => {
        var result = new Response();
        console.log(error.message);
        result.status = error.status;
        subject.next(result);
      }
    );
    return subject;
  }

  public editMaterial(body: any, idClass: number): Observable<Response> {
    var subject = new Subject<any>();
    this.getToken().subscribe((res) => {
      const headers = new HttpHeaders().set('authorization', res);
      this.httpClient
        .put(
          `${this.url}/materials-master/` + idClass + `?hasProject=false`,
          body,
          { headers: headers }
        )
        .subscribe(
          (data) => {
            var result = new Response();
            result.status = 'OK';
            result.data = data;
            subject.next(result);
          },
          (error) => {
            var result = new Response();
            console.log(error.message);
            result.status = error.status;
            subject.next(result);
          }
        );
    });
    return subject;
  }

  public getPieceSqft(body: any): Observable<Response> {
    var subject = new Subject<Response>();
    this.getToken().subscribe((res) => {
      const headers = new HttpHeaders().set('authorization', res);
      this.httpClient
        .post(`${this.url}/materials-master/getPieceSqFt`, body, {
          headers: headers,
        })
        .subscribe(
          (data) => {
            var result = new Response();
            result.status = 'OK';
            result.data = data;
            subject.next(result);
          },
          (error) => {
            var result = new Response();
            console.log(error.message);
            result.status = error.status;
            subject.next(result);
          }
        );
    });
    return subject;
  }

  public getSqFitPiece(body: any): Observable<Response> {
    var subject = new Subject<Response>();
    this.getToken().subscribe((res) => {
      const headers = new HttpHeaders().set('authorization', res);
      this.httpClient
        .post(`${this.url}/materials-master/getSqFitPiece`, body, {
          headers: headers,
        })
        .subscribe(
          (data) => {
            var result = new Response();
            result.status = 'OK';
            result.data = data;
            subject.next(result);
          },
          (error) => {
            var result = new Response();
            console.log(error.message);
            result.status = error.status;
            subject.next(result);
          }
        );
    });
    return subject;
  }

  public getPrice(sqfitpc:number, price:number): Observable<any> {
    var subject = new Subject<any>();
    this.getToken().subscribe((res) => {
      const headers = new HttpHeaders().set('authorization', res);
      this.httpClient
        .get(`${this.url}/materials-master/getPrice?sqFitPiece=`+sqfitpc+`&price=`+price, { headers: headers })
        .subscribe((data) => {
          subject.next(data);
        });
    });
    return subject.asObservable();
  }

}
