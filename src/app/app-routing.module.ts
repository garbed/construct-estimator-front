import { NgModule, ComponentFactoryResolver } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//master components
import { MaterialsComponent } from './components/materials/materials.component';
import { MaterialsClasificationsComponent } from './components/materials-clasifications/materials-clasifications.component';
import { MortarComponent } from './components/mortar/mortar.component';
import { OddCourseComponent } from './components/odd-course/odd-course.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { CrewComponent } from './components/crew/crew.component';

//project components

import {PMaterialsClassificationsComponent} from './project_components/p-materials-classifications/p-materials-classifications.component';
import {PMaterialsComponent} from './project_components/p-materials/p-materials.component';
import {PMortarComponent} from './project_components/p-mortar/p-mortar.component';



import { HomeComponent } from './home';
import { AdminComponent } from './admin';
import { LoginComponent } from './login';
import { AuthGuard } from './_helpers';
import { Role } from './_models';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin] },
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  { path: 'materials', component: MaterialsComponent },
  {
    path: 'materials-classification',
    component: MaterialsClasificationsComponent,
  },
  { path: 'mortar', component: MortarComponent },
  { path: 'odd-course', component: OddCourseComponent },
  { path: 'projects', component: ProjectsComponent },
  { path: 'crew', component: CrewComponent },
  { path: 'projectClass', component: PMaterialsClassificationsComponent },
  { path: 'projectMat', component: PMaterialsComponent },
  { path: 'projecttMortar', component: PMortarComponent },
  { path: '', component: ProjectsComponent },
  // otherwise redirect to home
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
