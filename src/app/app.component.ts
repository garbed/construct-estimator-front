import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './_services';
import { User, Role } from './_models';
import * as $ from 'jquery';
import { Project } from './components/projects/Project';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  currentUser: User;
  currentProject: Project;
  title = 'construct-estimator-front';

  ngOnInit(): void {
    $('#menu-toggle').click(function (e) {
      e.preventDefault();
      $('#wrapper').toggleClass('toggled');
    });
  }

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(
      (x) => (this.currentUser = x)
    );
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.Admin;
  }

  get isUser() {
    return this.currentUser && this.currentUser.role === Role.User;
  }

  setCurrentProject(projectP: Project){
     this.currentProject = projectP;
  }

 

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

 get  hasproject()  {

  let projectID = JSON.parse(localStorage.getItem('currentProjectID'));
  if(projectID != null){
    return true;
  }else{
    return false;
  }
  }
}
