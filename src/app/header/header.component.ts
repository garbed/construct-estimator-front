import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../_services';
import { User, Role } from '../_models';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  currentUser: User;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(
      (x) => (this.currentUser = x)
    );
  }

  ngOnInit(): void {}

  goHome(): void {
    localStorage.removeItem('currentProjectID');
    localStorage.removeItem('projectName');

  }

   getProjectName() {
    return JSON.parse(localStorage.getItem('projectName'));
  }

  hasName() {
    let pn =  JSON.parse(localStorage.getItem('projectName'));
    if(pn!=null){
        return  true;
    }else{
     return  false;
    }
  } 

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
