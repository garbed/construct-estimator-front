export const environment = {
  production: true,
  urlBack:
    'http://constructestimatorback-env.eba-jctaqapa.us-west-1.elasticbeanstalk.com',
  apiUrl: 'http://localhost:4000',
};
